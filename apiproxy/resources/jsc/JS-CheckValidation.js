var billingId = context.getVariable("request.queryparam.billingId");
var correlationId = context.getVariable("request.queryparam.correlationId");
var timeStamp = new Date().toISOString().replace('Z', '');
context.setVariable("timeStamp", timeStamp);
var billigIdRegExp = /^[0-9]/;

var jwt = context.getVariable("request.header.authorization");
context.setVariable("correlationId", correlationId);
if(jwt !== null){
    if(jwt.split(" ")[0]=="Bearer"){
        context.setVariable("request.header.authorization", jwt.split(" ")[1]);
    }
}

if(billingId === null) {
    context.setVariable("errorMessage", "Please provide the billingId.");
    context.setVariable("path", "/properties/note/properties/billingId/nonExisting");
    context.setVariable("isBillingValid", false);
}
else if(!billigIdRegExp.test(billingId)) {
    context.setVariable("errorMessage", "Please provide the numeric value for billingId.");
    context.setVariable("path", "/properties/note/properties/billingId/invalidValue");
    context.setVariable("isBillingValid", false);
}

else if (billingId.length < 4) {
    context.setVariable("errorMessage", "String is too short (" + billingId.length + " chars), minimum 4.");
    context.setVariable("path", "/properties/note/properties/billingId/minLength");
    context.setVariable("isBillingValid", false);
} 