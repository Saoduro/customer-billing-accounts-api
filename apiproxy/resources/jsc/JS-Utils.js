var requestVerb = context.getVariable("request.verb");
var timeStamp = new Date().toISOString().replace('Z', '');
context.setVariable("timeStamp", timeStamp);

if (requestVerb == "GET") {
    context.setVariable("correlationId", context.getVariable("request.queryparam.correlationId"));
} else {
    var bodyContent = context.getVariable('request.content');
    var body = JSON.parse(bodyContent);
    context.setVariable("correlationId", body.correlationId);
}