var billingId = context.getVariable("request.queryparam.billingId");
print(billingId);
var path =  "/billingaccounts/" + billingId + "/summary";
print(path);
context.setVariable("apiPath",path);

var headers = {};
headers.authorization = context.getVariable("request.header.authorization");

var http = {};
http.headers = headers;
http.method = context.getVariable("request.verb");
http.path = path;
http.audienceId = context.getVariable("private.audienceId");
http.pem = context.getVariable("private.pem");
print("pem value: ", http.pem);
http.keyMappingsUrl =  "http://"+context.getVariable("private.keymapping-eks-ip")+"/api/v2/customers/"+billingId+"/keylookup?idtype=BILLING_ID";

var request = {};
request.http = http;

var attributes = {};
attributes.request = request;

var opa_request = {};
opa_request.attributes = attributes;

context.setVariable("opa_request", JSON.stringify(opa_request));