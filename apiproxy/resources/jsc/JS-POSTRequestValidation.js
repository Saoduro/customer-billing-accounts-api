var bodyContent = context.getVariable('request.content');
var regex = new RegExp(/^[a-zA-Z0-9+]+([\._-][a-zA-Z0-9]+)*@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9]+){0,4}\.[a-zA-Z0-9]{1,4}$/);

var body = JSON.parse(bodyContent);
var email = body.email;
context.setVariable("isValidEmail", "true");    
context.setVariable("correlationId",body.correlationId);

if(typeof email != 'undefined' && !regex.test(email)) {
    context.setVariable("isValidRequest", "false");
    context.setVariable("errorMessage", "Please provide a valid email");
} else if (body.addresses.length > 1 ) {
    if(!body.addresses.some(address => address.type === "MAILING") || !body.addresses.some(address => address.type === "SERVICE")) {
        context.setVariable("isValidRequest", "false");
        context.setVariable("errorMessage",  "Missing MAILING or SERVICE address type");
    }     
}
else {
    context.setVariable("isValidRequest", "false");
    context.setVariable("errorMessage",  "Missing MAILING or SERVICE address type");
}



