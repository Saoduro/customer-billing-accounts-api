var response = context.getVariable("ServiceCallout.response");
print("error in javascript " + response);
var withoutQuotesResponse = response.replace(/"/g,"");
print(withoutQuotesResponse);
context.setVariable("isResponseValid", true);
context.setVariable("opa_response", withoutQuotesResponse);

if (withoutQuotesResponse === "Invalid JWT") {
    context.setVariable("opa_message", "Please provide a valid JWT.");
    context.setVariable("opa_error", "Unauthorized");
    context.setVariable("opa_status", "401");
    context.setVariable("isResponseValid", false);
    
} else if (withoutQuotesResponse === "false") {
    context.setVariable("opa_message", "Unauthorized User.");
    context.setVariable("opa_error", "Forbidden");
    context.setVariable("opa_status", "403");
    context.setVariable("isResponseValid", false);
}